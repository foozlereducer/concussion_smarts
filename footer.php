<div class="row row--dark-grey row--padding-medium footer">
	<footer class="row container-wide" role="contentinfo">
		<?php if ( !function_exists('dynamic_sidebar') || ! dynamic_sidebar('footer') ); ?>
	</footer>
</div>