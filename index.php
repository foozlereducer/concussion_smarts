<!doctype html>
<html prefix="og: http://ogp.me/ns#" lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Concussion Smarts - Know the facts - Education Service</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="Description" lang="en" content="">
	<!-- Schema.org markup for Google+ -->
	<meta itemprop="name" content="Concussion Smarts - Know the facts - Education Service">
	<meta itemprop="description" content="concussion smarts boosts your knowlege so you know what to do with head injuries">
	<meta itemprop="image" content="http://liviam.ca/img/logos/logo_bk.png">
	<!-- Twitter Card data -->
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@correlate-news">
	<meta name="twitter:title" content="Concussion Smarts - Know the facts - Education Service">
	<meta name="twitter:description" content="Concussion Education Service">
	<meta name="twitter:creator" content="@author_handle">
	<meta name="twitter:image:src" content="http://liviam.ca/concussion-smarts/img/logos/concussion_smarts_final_small.png">
	<!-- Open Graph meta information -->
	<meta property="og:title" content="Concussion Smarts">
	<meta property="og:type" content="website">
	<meta property="og:url" content="http://liviam.ca">
	<meta property="og:image" content="http://liviam.ca/concussion-smarts/img/logos/concussion_smarts_final_small.png">
	<meta property="og:description" content="concussion smarts boosts your knowlege so you know what to do with head injuries">
	<!-- Google Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:700,600,800,400|Old+Standard+TT:400,700' rel='stylesheet' type='text/css'>
	<!-- Apple touch icon links -->
	<link rel="icon" sizes="192x192" href="touch-icon-192x192.png">
	<link rel="apple-touch-icon-precomposed" sizes="180x180" href="apple-touch-icon-180x180-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="apple-touch-icon-152x152-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="apple-touch-icon-120x120-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="76x76" href="apple-touch-icon-76x76-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
	<!-- Favicon link -->
	<link rel="shortcut icon" href="/favicon.ico">
	<!-- IE tile icon links -->
	<meta name="msapplication-TileColor" content="#FFFFFF">
	<meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
	<meta name="msapplication-square310x310logo" content="img/ms-icon-310x310.png">
	<meta name="msapplication-wide310x150logo" content="img/ms-icon-310x150.png">
	<meta name="msapplication-square150x150logo" content="img/ms-icon-150x150.png">
	<meta name="msapplication-square70x70logo" content="img/ms-icon-70x70.png">
	<!-- CSS links -->
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/base.css">
	<link rel="stylesheet" href="css/layout.css">
	<link rel="stylesheet" href="css/modules.css">
	<!-- Header Javascript links -->
	<!--[if lt IE 9]>
		<script src="js/html5.js"></script>
		<script src="js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<!-- banner -->
<div class="row row--banner">
	<header class="row container-wide header" role="banner">
		<div class="header__logo">
			<img src="img/logos/logo_bk.png" alt="Concussion Smarts">
		</div>
		<nav class="header__nav" role="navigation">
			<ul>
				<li class="header__nav-bio"><a href="bio.html">Bio</a></li>
				<li class="header__nav-contact current"><a href="story.html">Blog</a></li>
			</ul>
		</nav>
	</header>
	<section class="row container-medium banner-content">
		<h1 class="banner-content__heading">
			Concussion Education Service
		</h1>
		<!--<a href="#"><img src="img/icon-cta.png" alt="App store"></a> -->
	</section>
</div>
<!-- freshest stories -->
<div class="row row--padding-wide fresh-stories">
	<div class="row container-wide">
			<div class="col-wide fresh-stories__story1">
				<h2 class="fresh-stories__heading">
					Story One
				</h2>
				<p>
					<img src='./img/x.jpg' />
					 <span>Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet.</span>
				</p>
			</div>
			<div class="col-wide fresh-stories__story2">
				<h2 class="fresh-stories__heading">
					Story Two
				</h2>
				<p>
				<img src='img/bell.jpg' />
				<span>Integer nec odio. Praesent libero. Sed cursus ante dapibus diam.</span> 

				</p>
			</div>
			<div class="col-wide fresh-stories__story3">
				<h2 class="fresh-stories__heading">
					Story Three
				</h2>
				<p>
					<img src='img/head.jpg' />
					<span>Taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</span>
				</p>
			</div>
		</div>
	</div>
</div>
<!-- features -->
<div class="row row--white row--padding-wide features">
	<div class="row container-medium">
		<div class="row features__row">
			<div class="col-narrow--right features__bike">
			</div>
			<div class="col-wide">
				<h2 class="features__heading">
					Easy To Get Started...
					Easy To Get Started...
					Easy To Get Started...
				</h2>
				<p class="features__text">
					Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. 
				</p>
			</div>
		</div>
		<div class="row features__row">
			<div class="col-narrow features__phone">
			</div>
			<div class="col-wide--right features__padding">
				<h2 class="features__heading">
					Facilitator Lead Sessions
				</h2>
				<p class="features__text">
					Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. 
				</p>			
			</div>
		</div>
		<div class="row features__row">
			<div class="col-narrow--right features__safe">
			</div>
			<div class="col-wide">
				<h2 class="features__heading">
					Training Packages
				</h2>
				<p class="features__text">
					Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. 
				</p>
			</div>
		</div>
	</div>
</div>

<!-- photos -->
<div class="row">
	<div class="col-medium">
		<img src="img/photo1.jpg" alt="Writing on notepaper">
	</div>
	<div class="col-medium">
		<img src="img/photo2.jpg" alt="Holding a mobile device showing a graph">
	</div>
</div>

<!-- testimonials -->
<div class="row row--white row--padding-wide testimonials">
	<section class="row container-narrow">
		<h3 class="testimonials__heading">
			What they are saying
		</h3>
		<img src="img/avatar.jpg" alt="Robert Johnson - Avatar">
		<blockquote>
			<p class="testimonials__quote">
				<em>"Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur"</em>
			</p>
			<p class="testimonials__source">
				Robert Johnson
			</p>
		</blockquote>
	</section>
</div>

<!-- facts -->
<div class="row row--grey row--padding-wide facts">
	<section class="row container-narrow facts-intro">
		<h3 class="facts-intro__heading">
			A few facts
		</h3>
		<p class="facts-intro__text">
			Lorem ipsum dolor sit amet, consectetur adipiscing metus elit.
		</p>
	</section>
	<div class="row container-medium facts-list">
		<div class="col-narrow">
			<span class="facts-list__icons facts-list__id"></span>
			<h4 class="facts-list__heading">2,000,000</h4>
			<p class="facts-list__text">Lorem ipsum</p>
		</div>
		<div class="col-narrow">
			<span class="facts-list__icons facts-list__eye"></span>
			<h4 class="facts-list__heading">11,000,000</h4>
			<p class="facts-list__text">Lorem ipsum</p>
		</div>
		<div class="col-narrow">
			<span class="facts-list__icons facts-list__timer"></span>
			<h4 class="facts-list__heading">5 minutes</h4>
			<p class="facts-list__text">Lorem ipsum</p>
		</div>
		<div class="col-narrow">
			<span class="facts-list__icons facts-list__chart"></span>
			<h4 class="facts-list__heading">40%</h4>
			<p class="facts-list__text">Lorem ipsum</p>
		</div>
	</div>
</div>

<!-- press -->
<div class="row row--blue row--padding-medium press">
	<section class="row container-wide">
		<h3 class="press__heading">
			Recent press
		</h3>
		<div class="row">
			<div class="press__logos press__logos--solvable">
				<a href="#"><img src="img/logo-solveable.png" alt="Solveable"></a>	
			</div>
			<div class="press__logos press__logos--nc">
				<a href="#"><img src="img/logo-nc.png" alt="NC"></a>
			</div>
			<div class="press__logos press__logos--waratah">
				<a href="#"><img src="img/logo-waratah.png" alt="The Waratah Post"></a>
			</div>
			<div class="press__logos press__logos--bevel">
				<a href="#"><img src="img/logo-bevel.png" alt="The Bevel"></a>
			</div>
		</div>
	</section>
</div>

<!-- footer -->
<div class="row row--dark-grey row--padding-medium footer">
	<footer class="row container-wide" role="contentinfo">
		<div class="col-wide footer__info">
			<div class="footer__logo">
				<img src="img/logos/concussion_smarts_final_small.png" alt="concussion smarts">
			</div>
			<div class="row">
				<div class="col-medium">
					<ul>
						<li><a href="#">About</a></li>
						<li><a href="#">Contact</a></li>
						<li><a href="#">FAQ</a></li>
					</ul>
				</div>
				<div class="col-medium">
					<ul>
						<li><a href="#">Facebook</a></li>
						<li><a href="#">Twitter</a></li>
						<li><a href="#">Google+</a></li>
					</ul>
				</div>
			</div>
			<div class="row footer__copyright">
				<ul>
					<li>&copy; 2015 Concussion Smarts inc.</li>
					<li><a href="#">Privacy Policy</a></li>
					<li><a href="#">Terms</a></li>
				</ul>
			</div>
		</div>
		<div class="col-narrow--right footer__cta">
			<a href="#"><img src="img/icon-cta.png" alt="App store"></a>
		</div>
	</footer>
</div>
</body>
</html>