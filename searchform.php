<form action="<?php bloginfo('siteurl'); ?>" id="searchform" method="get">
	<label for="s" class="screen-reader-text">Search for:</label>
	<span><input type="search" id="s" name="s" placeholder="Search" required />
	<input type="image" id="searchsubmit" alt="Search" src="<?php bloginfo('template_url'); ?>/img/searchicon.png" /></span>
</form>