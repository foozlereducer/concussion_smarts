<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#" lang="<?php language_attributes();?>">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php wp_title( '|' ); ?></title>
	<?php //if( 'mobile' == $sbmr_Device_Router->get_device() ) { ?>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<?php //} ?>

	<?php //if ( csmrt_is_404() ) { ?>
		<meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
	<?php //} ?>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="Description" lang="en" content="">
	<!-- Schema.org markup for Google+ -->
	<meta itemprop="name" content="Concussion Smarts - Know the facts - Education Service">
	<meta itemprop="description" content="concussion smarts boosts your knowlege so you know what to do with head injuries">
	<meta itemprop="image" content="http://liviam.ca/assets/img/logos/logo_bk.png">
	<!-- Twitter Card data -->
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@correlate-news">
	<meta name="twitter:title" content="Concussion Smarts - Know the facts - Education Service">
	<meta name="twitter:description" content="Concussion Education Service">
	<meta name="twitter:creator" content="@author_handle">
	<meta name="twitter:image:src" content="http://liviam.ca/concussion-smarts/assets/img/logos/concussion_smarts_final_small.png">
	<!-- Open Graph meta information -->
	<meta property="og:title" content="Concussion Smarts">
	<meta property="og:type" content="website">
	<meta property="og:url" content="http://liviam.ca">
	<meta property="og:image" content="http://liviam.ca/concussion-smarts/assets/img/logos/concussion_smarts_final_small.png">
	<meta property="og:description" content="concussion smarts boosts your knowlege so you know what to do with head injuries">
	<!-- Google Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:700,600,800,400|Old+Standard+TT:400,700' rel='stylesheet' type='text/css'>
	<!-- Apple touch icon links -->
	<link rel="icon" sizes="192x192" href="touch-icon-192x192.png">
	<link rel="apple-touch-icon-precomposed" sizes="180x180" href="apple-touch-icon-180x180-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="apple-touch-icon-152x152-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="apple-touch-icon-120x120-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="76x76" href="apple-touch-icon-76x76-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
	<!-- Favicon link -->
	<link rel="shortcut icon" href="/favicon.ico">
	<!-- IE tile icon links -->
	<meta name="msapplication-TileColor" content="#FFFFFF">
	<meta name="msapplication-TileImage" content="assets/img/ms-icon-144x144.png">
	<meta name="msapplication-square310x310logo" content="assets/img/ms-icon-310x310.png">
	<meta name="msapplication-wide310x150logo" content="assets/img/ms-icon-310x150.png">
	<meta name="msapplication-square150x150logo" content="assets/img/ms-icon-150x150.png">
	<meta name="msapplication-square70x70logo" content="assets/img/ms-icon-70x70.png">
	<!-- CSS links -->
	<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'stylesheet_url' ); ?>" >
	<!-- Header Javascript links -->
	<!--[if lt IE 9]>
		<script src="assets/js/html5.js"></script>
		<script src="assets/js/respond.min.js"></script>
	<![endif]-->
	<?php
		wp_head();
	?>
</head>